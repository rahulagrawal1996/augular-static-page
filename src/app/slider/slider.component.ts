import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css'],
  providers: [ NgbCarouselConfig ]
})
export class SliderComponent implements OnInit {

  images = ['../../assets/img/funstay_andaman__outside.jpg', '../../assets/img/funstay_andaman_outside.jpg', '../../assets/img/funstay_andaman_room_out.jpg','../../assets/img/funstay_andaman_room.jpg','../../assets/img/funstay_andaman_room1.jpg','../../assets/img/funstay_andaman_room3.jpg'];
  images_logo = ['../../assets/img/nerd-big.png','../../assets/img/puzzle.png','../../assets/img/money-bag.png']
  constructor(config: NgbCarouselConfig) {
    config.interval=2000;
    config.wrap = true;
    config.keyboard = true;
    config.pauseOnHover=false;
   }

  ngOnInit() {
  }

}
