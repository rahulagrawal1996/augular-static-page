import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
// import ngb module
import { NgbModule } from  '@ng-bootstrap/ng-bootstrap';
import { SliderComponent } from './slider/slider.component';
import { TopImageComponent } from './top-image/top-image.component';
import { PageDescriptionComponent } from './page-description/page-description.component';
import { BookNowComponent } from './book-now/book-now.component';
import { DetailsComponent } from './details/details.component';

@NgModule({
  declarations: [
    AppComponent,
    SliderComponent,
    TopImageComponent,
    PageDescriptionComponent,
    BookNowComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,NgbModule // add NgbModule here
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
