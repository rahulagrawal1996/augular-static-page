import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-image',
  templateUrl: './top-image.component.html',
  styleUrls: ['./top-image.component.css']
})


export class TopImageComponent implements OnInit {
  
  
  isImage:boolean=true;
  front_image="../../assets/img/funstay_andaman_outside.jpg";
  
  title_image={
    'background-image':'url('+this.front_image+')' , 'height' : '650px' , 'background-size' : 'cover'
  }
  title_video="../../assets/video/Boats-and-Waves.mp4"
  constructor() { }

  ngOnInit() { 
  }



  toggleVideo()
  {
    this.isImage=!this.isImage;
  }
  

}
