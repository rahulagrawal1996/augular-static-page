import { Component } from '@angular/core';
//npm install --save @ng-bootstrap/ng-bootstrap
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'staic-page-funstay';
}
